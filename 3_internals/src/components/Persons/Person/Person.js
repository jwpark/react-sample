import React, { Component } from "react";
import PropTypes from "prop-types";

import Aux from "../../../hoc/auxx";
import withClass from "../../../hoc/withClass";
import classes from "./Person.css";
import AuthContext from '../../../context/auth-context';

class Person extends Component {
  constructor(props) {
    super(props);
    this.inputElementRef = React.createRef();
    // console.log("----------------", this.inputElementRef);
  }
  
  static contextType = AuthContext;

  componentDidMount() {
    // console.log("==================", this.inputElementRef.current);
    // document.querySelector("input").focus();
    this.inputElementRef.current.focus();
    console.log(this.context.authenticated);
  }


  render() {
    console.log("[Person.js] rendering...");
    return (
      <Aux>
          {this.context.authenticated ? <p>Authenticated!</p> : <p>Please log in</p>}
          {/* <AuthContext.Consumer>
            {(context) => 
              context.authenticated ? <p>Authenticated!</p> : <p>Please log in</p>} */}
            {/* {this.props.isAuth ? <p>Authenticated!</p> : <p>Please log in</p>} */}
          {/* </AuthContext.Consumer> */}

          <p onClick={this.props.click}>
            I'm {this.props.name} and I am {this.props.age} years old!
          </p>
          <p key="i2">{this.props.children}</p>
          <input
            key="i3"
            // ref={inputEl => { this.inputElement = inputEl }}
            ref={this.inputElementRef}
            type="text"
            onChange={this.props.changed}
            value={this.props.name}
          />
      </Aux>
    );
  }
}

Person.propTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.number,
  changed: PropTypes.func
};

export default withClass(Person, classes.Person);
