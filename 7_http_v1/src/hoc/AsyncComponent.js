import React, { Component } from 'react';

const asyncComponent = (importcomponent) => {
    return class extends Component {
        state = {
            component: null
        }

        componentDidMount() {
            importcomponent()
            .then(cmp => {
                console.log("cmp is  = ", cmp)
                this.setState({component: cmp.default});
            });
        }

        render() {
            const C = this.state.component;
            console.log('...this.props is ', ...this.props);
            return C ? <C {...this.props} /> : null;
        }
    }
}

export default asyncComponent;