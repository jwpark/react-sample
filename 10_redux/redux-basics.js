const redux = require('redux');
const createStore = redux.createStore;

// Action : Action은 Store라는 저장소에 정보를 전달하기 위한 데이터 묶음이다.
const initialState = {
    counter: 0
}

// Reducer : 상태변화를 어떻게 시킬지 관리
const rootReducer = (state = initialState, action) => {
    if(action.type === 'INC_COUNTER') {
        return {
            ...state,
            counter: state.counter++
        };
    }
    if(action.type === 'ADD_COUNTER') {
        return {
            ...state,
            counter: state.counter + action.value
        };
    }
    return state;
};

// CreateStore
// Reducer들을 store안에 넣어주어서 서로 연결해준다.
const store = createStore(rootReducer);
console.log(store.getState());

// Subscription
//리덕스 스토어를 구독(subscribe)하는 작업은 나중에 react-redux의 connect함수가 대신한다
store.subscribe(() => {
    console.log('[Subscription]', store.getState());
});

// Dispatching Action
store.dispatch({type: 'INC_COUNTER'});
store.dispatch({type: 'ADD_COUNTER', value: 10});
console.log(store.getState())

