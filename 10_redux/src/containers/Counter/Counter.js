import React, { Component } from 'react';
import { connect } from 'react-redux';

import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';
import * as actionCreators from '../../store/actions/index';
// import * as actionTypes from '../../store/actions/actions';


class Counter extends Component {
    // state = {
    //     counter: 0
    // }

    counterChangedHandler = ( action, value ) => {
        switch ( action ) {
            case 'inc':
                this.setState( ( prevState ) => { return { counter: prevState.counter + 1 } } )
                break;
            case 'dec':
                this.setState( ( prevState ) => { return { counter: prevState.counter - 1 } } )
                break;
            case 'add':
                this.setState( ( prevState ) => { return { counter: prevState.counter + value } } )
                break;
            case 'sub':
                this.setState( ( prevState ) => { return { counter: prevState.counter - value } } )
                break;
        }
    }

    render () {
    //     console.log("this.state : ", this.state);
    // console.log("1243124123541312541 ", state.counter)
        return (
            <div>
                <CounterOutput value={this.props.ctr} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onIncrementCounter2}  />
                <CounterControl label="Add 5" clicked={this.props.onIncrementCounter3}  />
                <CounterControl label="Subtract 5" clicked={this.props.onIncrementCounter4}  />
                <hr />
                <button onClick={() => this.props.onStoreResult(this.props.ctr)}>Store Result</button>
                <ul>
                    {this.props.storedResults.map(strResult => (
                        <li key={strResult.id} onClick={() => this.props.onDeleteResult(strResult.id)}>{strResult.value}</li>
                    ))}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = state => {
    // console.log("this.state : ", this.state);
    // console.log("1243124123541312541 ", state.counter)
    return {
        ctr: state.ctr.counter,
        storedResults: state.res.results
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // onIncrementCounter: () => dispatch({type: actionTypes.INCREMENT}),
        // onIncrementCounter2: () => dispatch({type: actionTypes.DECREMENT}),
        // onIncrementCounter3: () => dispatch({type: actionTypes.ADD, val: 10}),
        // onIncrementCounter4: () => dispatch({type: actionTypes.SUBTRACT, val: 15}),
        // onStoreResult: (result) => dispatch({type: actionTypes.STORE_RESULT, result: result}),
        // onDeleteResult: (id) => dispatch({type: actionTypes.DELETE_RESULT, resultElId: id}),
        onIncrementCounter: () => dispatch(actionCreators.increment()),
        onIncrementCounter2: () => dispatch(actionCreators.decrement()),
        onIncrementCounter3: () => dispatch(actionCreators.add(10)),
        onIncrementCounter4: () => dispatch(actionCreators.subtract(15)),
        onStoreResult: (result) => dispatch(actionCreators.storeResult(result)),
        onDeleteResult: (id) => dispatch(actionCreators.deleteResult(id)),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Counter);