import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-16bda.firebaseio.com/',
});

export default instance;